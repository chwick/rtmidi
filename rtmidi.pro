TEMPLATE = lib
TARGET = RtMidi
CONFIG += staticlib c++11 noqt
CONFIG -= qt

DESTDIR = $$shadowed($$PWD)

SOURCES += RtMidi.cpp
HEADERS += RtMidi.h

linux:!android {
    # chose RtMidi backend
    DEFINES += __LINUX_ALSA__
}

winrt|winphone {}
else:win32:DEFINES += __WINDOWS_MM__
